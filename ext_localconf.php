<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('CeisUfro.Organigrama', 'Content');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['servicioOrganigrama'] = 'EXT:organigrama/Classes/EID/ServicioOrg.php';

