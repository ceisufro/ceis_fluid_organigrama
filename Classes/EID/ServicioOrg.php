<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 15-03-2016
 * Time: 11:14
 */


if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

error_log('-- SERVICIO TEST ORGANIGRAMA --');

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$respuesta = array(
        "name" => "Juan Perez",
        "title" => "General Manager",
        "children" => [
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador",
                "children" => [[
                    "name" => "drgwrgw",
                    "title" => "Desarrollador"
                ]]],
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador"],
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador",
                "children" => [[
                    "name" => "drgwrgw",
                    "title" => "Desarrollador"
                ],
                    [
                        "name" => "drgwrgw",
                        "title" => "Desarrollador"
                    ],
                    [
                        "name" => "drgwrgw",
                        "title" => "Desarrollador"
                    ]]],
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador"],
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador"],
            [
                "name" => "drgwrgw",
                "title" => "Desarrollador"]
            ]
);

echo json_encode($respuesta);