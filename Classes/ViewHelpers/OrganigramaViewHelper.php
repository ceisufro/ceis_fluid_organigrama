<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 30-05-16
 * Time: 15:13
 */

namespace CeisUfro\Organigrama\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class OrganigramaViewHelper extends AbstractFormViewHelper {

    /**
     * 
     * @param string $enlace
     * @param array $param parametros opcionales
     * @return string
     */
    public function render($enlace, array $param = NULL ) {
        $directorioVista = PATH_site."typo3conf/ext/organigrama/Resources/Private/Templates/VistaGrafico/";
        $rutaCompletaVista = $directorioVista.'organigrama.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        if(empty($enlace)) {
            $mensaje = "Sin datos externos ";
        } else {

            //Obtiene datos externos desde servicio
            $json = $this->wsRestRequest("POST", $enlace, $param);

            $vista->assign("json",$json);
            $conenidoJson = json_decode($json,true);
            $vista->assign('contenidoJson',$conenidoJson);
        }

        $vista->assign('mensaje',$mensaje);

        return $vista->render();
    }

    /**
     * Function to call a JSON REST Service
     *
     * @param string $method HTTP method: "GET"(default), "POST", "PUT" or "DELETE"
     * @param string $url dirección servicio
     * @param array $query_params Optional. Associative array containing the variables to use in the request if "POST" or "PUT" method.
     * @return mixed
     */
    private function wsRestRequest($method = "GET", $url, $query_params = NULL) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $method = strtoupper($method);

        if($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
        }elseif($method == "DELETE" || $method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            if($query_params !== NULL) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query_params));
            }
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}