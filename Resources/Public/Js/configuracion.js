/**
 * Created by Javier on 30-05-16.
 */
function recibeJsonOrg() {
    var inputJson = $("#campoRecibeJsonOrg");
    if(inputJson.val() != undefined ) {

        if (inputJson.val().length != 0) {
            var json = JSON.parse(inputJson.val());
            return json;
        } else {
            return 0;
        }

    }else{
        return 0;
    }
}

if (recibeJsonOrg() != 0) {
    'use strict';
    (function($){
        $(function() {

            var datasource = recibeJsonOrg();

            $('#contenedor_organigrama').orgchart({
                'data' : datasource,
                'depth': 10,
                'nodeContent': 'title',
                'draggable': true
            });
        });
    })(jQuery);
}

